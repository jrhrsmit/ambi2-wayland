#include "serial.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

// prototypes for local functions
void set_blocking(int fd, int should_block);
int set_interface_attribs(int fd, int speed, int parity);

static int serial_file = -1;

int serial_get_port(void)
{
	return serial_file;
}

#define READBUF_SIZE 1024
static char readbuf[READBUF_SIZE] = {0};

int serial_readline(char *data, int len)
{
	int res = read(serial_file, readbuf, READBUF_SIZE - 1);
	if (res < 0) {
		fprintf(stderr, "Error reading from serial port\n");
		exit(1);
	}

	char *pch = strrchr(readbuf, '\n');
	if (pch == NULL) {
		return 0;
	}
	int strlen = pch - readbuf + 1;
	if (strlen > len) {
		fprintf(stderr, "result does not fit in supplied array");
		exit(1);
	}
	char scratchpad[READBUF_SIZE] = {0};
	// copy all complete lines to data and terminate
	memcpy(data, readbuf, strlen);
	data[strlen] = '\0';
	// move remaining data to start of readbuf
	memcpy(scratchpad, &readbuf[strlen], READBUF_SIZE - strlen);
	memset(readbuf, 0, READBUF_SIZE);
	memcpy(readbuf, scratchpad, READBUF_SIZE - strlen);
	return strlen;
}

int serial_write(char *data, int len)
{
	//	printf("[%s]: Writing: \"%s\"\n", __func__, data);
	//	fflush(stdout);
	write(serial_file, (uint8_t *)data, len);
	return 0;
}

void serial_open_port(char port[])
{
	if (strlen(port) > 512) {
		fprintf(stderr, "error: port argument too large, use a real port\n");
		exit(1);
	}
	serial_file = open(port, O_RDWR | O_NOCTTY | O_SYNC);
	if (serial_file < 0) {
		fprintf(stderr, "error %d opening %s: %s \n", errno, port, strerror(errno));
		exit(1);
	}
	printf("Successfully opened serial port %s!\n", port);
	// set speed to 230400 bps, 8n1 (no parity)
	// set_interface_attribs(serial_file, B230400, 0);
	set_interface_attribs(serial_file, B115200, 0);
	// set blocking
	set_blocking(serial_file, 0);
}

int set_interface_attribs(int fd, int speed, int parity)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		printf("error %d from tcgetattr\n", errno);
		return -1;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK; // disable break processing
	tty.c_lflag = 0;	// no signaling chars, no echo,
	// no canonical processing
	tty.c_oflag = 0; // no remapping, no delays

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD); // ignore modem controls,
	// enable reading
	tty.c_cflag &= ~(PARENB | PARODD); // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		printf("error %d from tcsetattr: %s\n", errno, strerror(errno));
		return -1;
	}
	return 0;
}

void set_blocking(int fd, int should_block)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		printf("error %d from tggetattr\n", errno);
		return;
	}

	tty.c_cc[VMIN] = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 1; // 0.1 seconds read timeout

	if (tcsetattr(fd, TCSANOW, &tty) != 0)
		printf("error %d setting term attributes\n", errno);
}
