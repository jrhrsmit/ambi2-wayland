// Adapted from
// https://stackoverflow.com/questions/34511312/how-to-encode-a-video-from-several-images-generated-in-a-c-program-without-wri
// (Later) adapted from https://github.com/apc-llc/moviemaker-cpp

#ifndef FRAME_WRITER
#define FRAME_WRITER

#include "config.h"

#include <stdint.h>

#include <atomic>
#include <map>
#include <string>
#include <vector>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libswresample/swresample.h>
#ifdef HAVE_LIBAVDEVICE
#include <libavdevice/avdevice.h>
#endif

#include "averr.h"
#include "serial.h"

#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/common.h>
#include <libavutil/display.h>
#include <libavutil/hwcontext.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
}

#include "config.h"

enum InputFormat {
	INPUT_FORMAT_BGR0,
	INPUT_FORMAT_RGB0,
	INPUT_FORMAT_BGR8,
	INPUT_FORMAT_RGB565,
	INPUT_FORMAT_BGR565,
	INPUT_FORMAT_X2RGB10,
	INPUT_FORMAT_X2BGR10,
};

struct FrameWriterParams {
	std::string file;
	int width;
	int height;
	int stride;

	int brightness = 16;
	int strip_width;
	int strip_height;
	int strip_num_leds;
	char *port;

	InputFormat format;

	std::string video_filter = "null"; // dummy filter

	std::string codec;
	std::string audio_codec;
	std::string muxer;
	std::string pix_fmt;
	std::string sample_fmt;
	std::string hw_device; // used only if codec contains vaapi
	std::map<std::string, std::string> codec_options;
	std::map<std::string, std::string> audio_codec_options;
	int framerate;
	int sample_rate;

	int64_t audio_sync_offset;

	bool enable_audio;
	bool enable_ffmpeg_debug_output;

	int bframes;

	std::atomic<bool> &write_aborted_flag;
	FrameWriterParams(std::atomic<bool> &flag) : write_aborted_flag(flag)
	{
	}
};

class FrameWriter
{

	FrameWriterParams params;
	void load_codec_options(AVDictionary **dict);
	void load_audio_codec_options(AVDictionary **dict);

	const AVOutputFormat *outputFmt;
	AVStream *videoStream;
	AVCodecContext *videoCodecCtx;
	AVFormatContext *fmtCtx;

	AVFilterContext *videoFilterSourceCtx = NULL;
	AVFilterContext *videoFilterPixelizeCtx = {NULL};
	AVFilterContext *videoFilterSinkCtx = NULL;
	AVFilterGraph *videoFilterGraph = NULL;

	AVBufferRef *hw_device_context = NULL;
	AVBufferRef *hw_frame_context = NULL;

	AVPixelFormat lookup_pixel_format(std::string pix_fmt);
	AVPixelFormat handle_buffersink_pix_fmt(const AVCodec *codec);
	AVPixelFormat get_input_format();
	void init_hw_accel();
	void init_codecs();
	void init_video_filters(const AVCodec *codec);
	void init_video_stream();

	void encode(AVCodecContext *enc_ctx, AVFrame *frame, AVPacket *pkt);

	void finish_frame(AVCodecContext *enc_ctx, AVPacket &pkt);

	/**
	 * Upload data to a frame.
	 */
	AVFrame *prepare_frame_data(const uint8_t *pixels, bool y_invert);

      public:
	void ambi_init();
	void ambi(AVFrame *frame);
	FrameWriter(const FrameWriterParams &params);
	bool add_frame(const uint8_t *pixels, int64_t usec, bool y_invert);

	~FrameWriter();
};

#include <atomic>
#include <memory>
#include <mutex>

extern std::mutex frame_writer_mutex, frame_writer_pending_mutex;
extern std::unique_ptr<FrameWriter> frame_writer;
extern std::atomic<bool> exit_main_loop;

#endif // FRAME_WRITER
