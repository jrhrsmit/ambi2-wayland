#ifndef AMBI_SERIAL_H_
#define AMBI_SERIAL_H_

void serial_open_port(char port[]);
int serial_get_port(void);
int serial_write(char *data, int len);
int serial_readline(char *data, int len);

#endif // AMBI_SERIAL_H_
