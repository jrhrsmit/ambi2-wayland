#!/bin/python

import sys
import getopt
import serial
import time
import csv
import colorsys
import pyscreenshot as ImageGrab


def print_opt(opt, desc):
    width = 30
    print(f"\t{opt:<{width}}{desc}")


def print_help_exit():
    print("Usage: ./ambi.py -p <port> [OPTIONS]")
    print("Options:")
    print_opt(
        "-p <port>", "Connect to the led strip controller via serial port <port>")
    print_opt("-b <baudrate>", "Set the baud rate of the serial port")
    print_opt("-t <timeout>", "Set the timeout in seconds of the serial port")
    print_opt("-r <bit depth>", "Set the render bit depth. Defaults to 8")
    print_opt("-n <strip num leds>", "Set the number of leds in the LED strip."
              "Defaults to 40")
    print_opt("-h", "Prints this help message")
    print("Example:")
    print("\t./ambi.py -p /dev/ttyACM0")
    sys.exit(1)


def serial_read(ser):
    # Read any output from the controller
    res = 0
    while ser.in_waiting > 0:
        s = ser.readline().decode("utf-8")
        # s = ser.read(ser.in_waiting).decode("utf-8")
        if s == "" or s == None:
            break
        res = s.strip('\n')
        print(f"[led_strip_controller]: {res}")
        if "<err>" in s or "<wrn>" in s:
            res = -1
    return res


def serial_write(ser, string, attempts=3):
    # convert to bytes, add enough return and newlines
    data = str.encode(string + "\r")

    for i in range(attempts):
        serial_read(ser)
        # ser.flushInput()
        # write data
        try:
            ser.write(data)
        except serial.serialutil.SerialTimeoutException:
            print("Write timeout.", file=sys.stderr)
            # ser.close()
            sys.exit(1)

        # fetch response
        s = ser.readline().decode("utf-8").strip("\r\n")
        res = s.split(".")
        print(f"[led_strip_controller]: {s}")
        echo_cmd = res[0]
        cmd = string.strip(" ")[0]
        try:
            Q = int(res[1])
        except:
            Q = -1
            pass

        if Q > 1:
            print(f"{cmd}, Q: {Q}")

        if echo_cmd == cmd:
            return 0

        print(f"Echoed cmd is not the same as written cmd (attempt"
              f" {i}).\n\tSent:\t\"{cmd}\","
              f"\n\tgot:\t\"{echo_cmd}\"", file=sys.stderr)

    return -1

def ambi(ser, strip_num_leds, tgt_frequency):
    while True:
        t_frame = time.time()
        im = ImageGrab.grab(backend="grim")
        ser.flushInput()
        cmd = "a"
        for led in range(strip_num_leds):
            color = colorsys.hsv_to_rgb(
                ((led + frame) % strip_num_leds)/strip_num_leds, 1, 0.1)
            r = int(color[0] * 0xff)
            g = int(color[1] * 0xff)
            b = int(color[2] * 0xff)
            cmd = cmd + f" {r:02x}{g:02x}{b:02x}"

        if serial_write(ser, cmd):
            print(f"Could not write colors for frame {frame}")

        while (time.time() - t_frame) < 1/tgt_frequency:
            time.sleep(0.00001)

def rainbow(ser, strip_num_leds, tgt_frequency):
    t_start = time.time()

    # use set_led_all
    num_frames = 60*100000
    for frame in range(num_frames):
        # ser.flushInput()
        t_frame = time.time()
        cmd = "a"
        for led in range(strip_num_leds):
            color = colorsys.hsv_to_rgb(
                ((led + frame) % strip_num_leds)/strip_num_leds, 1, 0.1)
            r = int(color[0] * 0xff)
            g = int(color[1] * 0xff)
            b = int(color[2] * 0xff)
            cmd = cmd + f" {r:02x}{g:02x}{b:02x}"

        if serial_write(ser, cmd):
            print(f"Could not write colors for frame {frame}")

        while (time.time() - t_frame) < 1/tgt_frequency:
            time.sleep(0.00001)

    t_end = time.time()

    t_diff = t_end - t_start

    print(f"Time to process {num_frames} frames: {t_diff:.3} s")
    print(f"Time per frame {t_diff/num_frames:.3} s")
    print(f"Frequency {num_frames/t_diff:.3} Hz")


def main(argv):
    # defaults
    test = False
    port = ''
    render_bit_depth = 8
    strip_num_leds = 55
    baudrate = 115200
    timeout = 0.1
    reboot = False
    tgt_frequency = 60

    # fetch the arguments
    try:
        opts, args = getopt.getopt(argv, "hp:r:n:b:t:", ["port=",
                                                         "strip-num-leds=",
                                                         "render-bit-depth=",
                                                         "timeout=",
                                                         "baudrate=", "reboot"])
    except getopt.GetoptError as err:
        print(err, file=sys.stderr)
        print_help_exit()

    # parse the arguments
    for opt, arg in opts:
        if opt == "-h":
            print_help_exit()
        elif opt == "-p" or opt == "port=":
            port = arg
        elif opt == "-n" or opt == "strip-num-leds=":
            try:
                strip_num_leds = int(arg)
            except:
                print(f"Could not parse {arg} as int.", file=sys.stderr)
                print_help_exit()
        elif opt == "-r" or opt == "render-bit-depth=":
            try:
                render_bit_depth = int(arg)
            except:
                print(f"Could not parse {arg} as int.", file=sys.stderr)
                print_help_exit()
        elif opt == "-b" or opt == "baudrate=":
            try:
                baudrate = int(arg)
            except:
                print(f"Could not parse {arg} as int.", file=sys.stderr)
                print_help_exit()
        elif opt == "-t" or opt == "timeout=":
            try:
                timeout = float(arg)
            except:
                print(f"Could not parse {arg} as float.", file=sys.stderr)
                print_help_exit()
        elif opt == "reboot":
            reboot = True
        else:
            print_help_exit()

    write_timeout = 5 * timeout

    # print help if the serial port is not set
    if port == '':
        print_help_exit()

    # try to open the serial port
    ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout,
                        write_timeout=timeout)

    # Write something to stop the possible execution of the led_set_range
    # command
    serial_write(ser, f"recover")
    time.sleep(0.5)
    serial_read(ser)

    if reboot:
        # Reboot
        serial_write(ser, f"reboot")
        ser.close()

        time.sleep(2)
        ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        time.sleep(2)
        serial_read(ser)

        serial_write(ser, f"recover")
        time.sleep(0.5)
        serial_read(ser)

    if serial_write(ser, f"led_set_brightness 2"):
        print("Could not set brightness.", file=sys.stderr)

    # initialize the LED strip
    attempts = 3
    for i in range(attempts):
        if serial_write(ser, f"led_init {strip_num_leds} {render_bit_depth}"):
            time.sleep(0.5)
            continue
        time.sleep(1)
        if serial_read(ser) != 0:
            if i < attempts - 1:
                continue
            else:
                print(
                    "Could not initialize LED strip after {retries} attempts.", file=sys.stderr)
                ser.close()
                sys.exit(1)

    serial_read(ser)

    if test:
        rainbow(ser, strip_num_leds, tgt_frequency)
    else:
        rainbow(ser, strip_num_leds, tgt_frequency)
        #ambi(ser, strip_num_leds, tgt_frequency)

    ser.close()
    sys.exit(0)
    


if __name__ == "__main__":
    main(sys.argv[1:])
