#pragma once

void led_set(int argc, char** argv);
void led_set_all(int argc, char** argv);
void led_set_range(int argc, char** argv);
void led_set_brightness(int argc, char** argv);
void led_strip_update(int argc, char** argv);
void led_print_colors(int argc, char** argv);
void led_init(int argc, char** argv);
