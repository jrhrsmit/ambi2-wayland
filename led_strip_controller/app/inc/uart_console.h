#pragma once

#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#define UART_CONSOLE_COMMAND_LEN 32
#define UART_CONSOLE_ARGS_LEN 24
#define UART_CONSOLE_DESCRIPTION_LEN 128
#define UART_CONSOLE_LINE_LEN 2048
#define UART_CONSOLE_MAX_ARGS 200

typedef struct {
    const char command[UART_CONSOLE_COMMAND_LEN];
    const char args[UART_CONSOLE_ARGS_LEN];
    const char description[UART_CONSOLE_DESCRIPTION_LEN];
    void (*cmd_fn)(int, char**);
} uart_console_cmd_t;

struct cmd_fifo_data {
    intptr_t reserved;
    char* cmd;
};

// device descritpion
struct uart_console_t {
    k_tid_t tid;
    uart_console_cmd_t* commands;
    int num_commands;
    struct k_fifo cmd_fifo;
    volatile int cmd_fifo_queue_len;
};

struct uart_console_t* uart_console_get(void);
int uart_console_init(uart_console_cmd_t commands[], int num_commands);
