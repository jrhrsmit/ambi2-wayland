#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <zephyr/console/console.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include "uart_console.h"

#include <zephyr/logging/log.h>
#include <zephyr/logging/log_ctrl.h>
#include <zephyr/sys/arch_interface.h>

LOG_MODULE_REGISTER(uart_console, LOG_LEVEL_INF);

#define UART_CONSOLE_STACK_SIZE 16192
#define UART_CONSOLE_PRIORITY K_HIGHEST_APPLICATION_THREAD_PRIO

static struct uart_console_t uart_console;

struct uart_console_t* uart_console_get(void) {
    return &uart_console;
}

static int _parse_cmd(char* token_ptr, uart_console_cmd_t* cmd, char* argv[]) {
    int num_args_loaded = 0;
    // fetch first argument
    token_ptr = (char*)strtok(NULL, " ");
    // for each token, convert the argument to an int
    for (num_args_loaded = 0;
         token_ptr != NULL && num_args_loaded < UART_CONSOLE_MAX_ARGS;
         num_args_loaded++) {
        // append the argument to the argument list
        argv[num_args_loaded] = token_ptr;
        // load the next token
        token_ptr = (char*)strtok(NULL, " ");
    }
    LOG_DBG("Read %d args", num_args_loaded);
    return num_args_loaded;
}

static void uart_console_cmd_thread(void* p1, void* p2, void* p3) {
    int i;
    char* token_ptr;
    char* argv[UART_CONSOLE_MAX_ARGS];

    struct uart_console_t* dev = p1;

    while (true) {
        char* cmd = k_fifo_get(&dev->cmd_fifo, K_FOREVER);
        dev->cmd_fifo_queue_len--;
        LOG_DBG("Processing command: %s", cmd);

        // tokenize input
        token_ptr = (char*)strtok(cmd, " ");
        if (token_ptr == NULL) {
            LOG_ERR("Could not parse input");
        } else {
            // compare input to available commands, and run their funciton if
            // it's a match
            for (i = 0; i < dev->num_commands; i++) {
                if (strcmp(token_ptr, dev->commands[i].command) == 0) {
                    int argc = _parse_cmd(token_ptr, &dev->commands[i], argv);
                    dev->commands[i].cmd_fn(argc, argv);
                    LOG_DBG("Command \"%s\" completed.", token_ptr);
                    break;
                }
            }
            // If none of the commands matched, return an error
            if (i == dev->num_commands) {
                LOG_ERR("Unknown command: \"%s\"", token_ptr);
                LOG_INF("Type \"help\" for a list of available commands.");
            }
        }
        k_free(cmd);
    }
}

K_THREAD_STACK_DEFINE(uart_console_stack_area, UART_CONSOLE_STACK_SIZE);
struct k_thread uart_console_thread_data;

int uart_console_init(uart_console_cmd_t commands[], int num_commands) {
    struct uart_console_t* dev = uart_console_get();
    dev->commands = commands;
    dev->num_commands = num_commands;
    dev->cmd_fifo_queue_len = 0;
    k_fifo_init(&dev->cmd_fifo);

    dev->tid = k_thread_create(
        &uart_console_thread_data, uart_console_stack_area,
        K_THREAD_STACK_SIZEOF(uart_console_stack_area), uart_console_cmd_thread,
        dev, NULL, NULL, UART_CONSOLE_PRIORITY, K_ESSENTIAL, K_FOREVER);

    // k_thread_cpu_mask_clear(dev->tid);
    // k_thread_cpu_pin(dev->tid, 1);
    k_thread_start(dev->tid);

    LOG_INF(
        "UART console initialized. Type \"help\" for a list of "
        "commands.");
    return 0;
}
