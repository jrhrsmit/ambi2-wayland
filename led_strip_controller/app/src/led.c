#include <limits.h>
#include <stddef.h>
#include <stdlib.h>

#include <zephyr/console/console.h>
#include <zephyr/drivers/led_strip.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>
#include <zephyr/logging/log_ctrl.h>
LOG_MODULE_REGISTER(led, LOG_LEVEL_INF);

#include "led.h"
#include "uart_console.h"
#include "gamma.h"

const struct device* strip = DEVICE_DT_GET_ANY(apa_apa102);

static int render_bit_depth = 8;

static int strip_num_leds = 0;
static struct led_rgb* strip_colors = NULL;
static uint8_t brightness = 0x1F;

struct apa102_config {
    struct spi_dt_spec bus;
};

static int apa102_update(const struct device* dev, void* buf, size_t size) {
    const struct apa102_config* config = dev->config;
    static const uint8_t zeros[] = {0, 0, 0, 0};
    static const uint8_t ones[] = {0xFF, 0xFF, 0xFF, 0xFF};
    const struct spi_buf tx_bufs[] = {
        {
            /* Start frame: at least 32 zeros */
            .buf = (uint8_t*)zeros,
            .len = sizeof(zeros),
        },
        {
            /* LED data itself */
            .buf = buf,
            .len = size,
        },
        {
            /* End frame: at least 32 ones to clock the
             * remaining bits to the LEDs at the end of
             * the strip.
             */
            .buf = (uint8_t*)ones,
            .len = sizeof(ones),
        },
    };
    const struct spi_buf_set tx = {.buffers = tx_bufs,
                                   .count = ARRAY_SIZE(tx_bufs)};

    return spi_write_dt(&config->bus, &tx);
}

static int apa102_update_rgb(const struct device* dev,
                             struct led_rgb* pixels,
                             size_t count) {
    uint8_t* p = (uint8_t*)pixels;
    size_t i;
    /* SOF (3 bits) followed by the 0 to 31 global dimming level */
    uint8_t prefix = 0xE0 | brightness;

    /* Rewrite to the on-wire format */
    for (i = 0; i < count; i++) {
        uint8_t r = pixels[i].r;
        uint8_t g = pixels[i].g;
        uint8_t b = pixels[i].b;

        *p++ = prefix;
        *p++ = b;
        *p++ = g;
        *p++ = r;
    }

    BUILD_ASSERT(sizeof(struct led_rgb) == 4);
    return apa102_update(dev, pixels, sizeof(struct led_rgb) * count);
}

static int _arg_to_int(char* arg, int32_t* res) {
    int32_t converted;

    converted = strtol(arg, NULL, 10);

    // check if the int is within bounds
    if (converted >= INT_MAX || converted <= INT_MIN) {
        LOG_ERR(
            "Could not convert decimal string to int. Read: \"%s\", "
            "value: %d",
            arg, converted);
        return -1;
    }

    *res = converted;
    return 0;
}

static int _arg_to_color(char* arg, struct led_rgb* res) {
    int32_t converted = 0;

    if (strlen(arg) != 6) {
        LOG_ERR("Could not convert hex string to color. Read: \"%s\"", arg);
        return -1;
    }

    for (int i = 0; i < 6; i++) {
        if (arg[i] >= '0' && arg[i] <= '9')
            converted |= arg[i] - '0';
        else if (arg[i] >= 'a' && arg[i] <= 'f')
            converted |= arg[i] - 'a' + 0xA;
        else if (arg[i] >= 'A' && arg[i] <= 'F')
            converted |= arg[i] - 'A' + 0xA;
        else {
            LOG_ERR("Could not convert hex string to color. Read: \"%s\"", arg);
            return -1;
        }
        if (i < 5)
            converted <<= 4;
    }

    res->r = (converted >> (render_bit_depth * 2)) & 0xFF;
    res->g = (converted >> (render_bit_depth)) & 0xFF;
    res->b = converted & 0xFF;
	
	int shift = 12 - render_bit_depth;
	res->r = gamma_lut[res->r << shift];
	res->g = gamma_lut[res->g << shift];
	res->b = gamma_lut[res->b << shift];
    LOG_DBG("Color: %s is %06X (r %02X, g %02X, b %02X)", arg, converted,
            res->r, res->g, res->b);
    return 0;

    converted = strtol(arg, NULL, 16);

    // check if the int is within bounds
    if (converted >= INT_MAX || converted <= INT_MIN) {
        LOG_ERR(
            "Could not convert hex string to int32_t. Read: \"%s\", value: %d",
            arg, converted);
        return -1;
    }
}

void led_set(int argc, char** argv) {
    uint32_t num_led;

    if (argc != 2) {
        LOG_ERR("Incorrect number of arguments for %s. Expected 2, got %d",
                __func__, argc);
        return;
    }

    if (_arg_to_int(argv[0], &num_led))
        return;
    if (_arg_to_color(argv[1], &strip_colors[num_led]))
        return;

    if (num_led > strip_num_leds - 1) {
        LOG_ERR("LED index (%d) would be outside of the strip (%d LEDs).",
                strip_num_leds, num_led);
        return;
    }

    LOG_DBG("LED %3d: %02X %02X %02X", num_led, strip_colors[num_led].r,
            strip_colors[num_led].g, strip_colors[num_led].b);

    // led_strip_update(0, NULL);
}

void led_set_all(int argc, char** argv) {
    if (argc != strip_num_leds) {
        LOG_ERR("Incorrect number of arguments for %s. Expected 0, got %d",
                __func__, argc);
        return;
    }

    for (int i = 0; i < strip_num_leds; i++) {
        int err = _arg_to_color(argv[i], &strip_colors[i]);

        if (err) {
            LOG_ERR("Could not process color for LED %d", i);
        }
        LOG_DBG("LED %3d: %02X %02X %02X", i, strip_colors[i].r,
                strip_colors[i].g, strip_colors[i].b);
    }

    led_strip_update(0, NULL);
}

void led_set_range(int argc, char** argv) {
    int num_leds;
    int start_led;

    if (argc != 2) {
        LOG_ERR("Incorrect number of arguments for %s. Expected 2, got %d",
                __func__, argc);
        return;
    }

    if (_arg_to_int(argv[0], &start_led))
        return;
    if (_arg_to_int(argv[1], &num_leds))
        return;

    if (start_led + num_leds > strip_num_leds) {
        LOG_ERR(
            "End of range (index %d) would be outside of the strip (%d LEDs).",
            start_led + num_leds, strip_num_leds);
        return;
    }

    for (int i = 0; i < num_leds; i++) {
        struct uart_console_t* dev = uart_console_get();
        char* cmd = k_fifo_get(&dev->cmd_fifo, K_FOREVER);
        dev->cmd_fifo_queue_len--;
        LOG_DBG("Processing command: %s", cmd);
        int err = _arg_to_color(cmd, &strip_colors[start_led + i]);
        k_free(cmd);

        if (err) {
            LOG_ERR("Could not process color for LED %d", i);
            return;
        }
        LOG_DBG("LED %3d: %02X %02X %02X", i, strip_colors[i].r,
                strip_colors[i].g, strip_colors[i].b);
    }

    led_strip_update(0, NULL);
}

static struct led_rgb* leds = NULL;
static void led_strip_update_work_handler(struct k_work* work) {
    memcpy(leds, strip_colors, sizeof(struct led_rgb) * strip_num_leds);
	apa102_update_rgb(strip, leds, strip_num_leds);
}

K_WORK_DEFINE(led_strip_update_work, led_strip_update_work_handler);

void led_strip_update(int argc, char** argv) {
    if (k_work_is_pending(&led_strip_update_work)) {
        LOG_WRN("SPI drop 1 frame");
        return;
    }

    k_work_submit(&led_strip_update_work);
}

void led_print_colors(int argc, char** argv) {
    for (int i = 0; i < strip_num_leds; i++) {
        LOG_INF("LED %3d: %02X %02X %02X", i, strip_colors[i].r,
                strip_colors[i].g, strip_colors[i].b);
    }
}

void led_set_brightness(int argc, char** argv) {
    int brt, err;
    int brt_max = (1 << (5 - (8 - render_bit_depth))) - 1;
	int brt_min = render_bit_depth == 8;
    err = _arg_to_int(argv[0], &brt);
    if (err)
        return;
    if (brt < brt_min || brt > brt_max) {
        LOG_ERR("Brightness must be between %d and %d.", brt_min, brt_max);
    }
    brightness = brt;
}

void led_init(int argc, char** argv) {
    if (argc != 2) {
        LOG_ERR("Incorrect number of arguments for %s. Expected 2, got %d",
                __func__, argc);
        return;
    }

    if (_arg_to_int(argv[0], &strip_num_leds))
        return;
    if (_arg_to_int(argv[1], &render_bit_depth))
        return;

    if (render_bit_depth != 8 && render_bit_depth != 10 &&
        render_bit_depth != 12) {
        LOG_ERR("Render bit depth must be 8, 10, or 12");
        return;
    }

    if (!strip) {
        LOG_ERR("LED strip device not found");
        return;
    } else if (!device_is_ready(strip)) {
        LOG_ERR("LED strip device %s is not ready", strip->name);
        return;
    } else {
        LOG_DBG("Found LED strip device %s", strip->name);
    }

    k_free(leds);
    leds = (struct led_rgb*)k_calloc(sizeof(struct led_rgb), strip_num_leds);
    if (!leds) {
        LOG_ERR("Could not allocate memory for LEDs");
        return;
    }

    k_free(strip_colors);
    strip_colors =
        (struct led_rgb*)k_calloc(sizeof(struct led_rgb), strip_num_leds);
    if (!strip_colors) {
        LOG_ERR("Could not allocate memory for strip colors");
        return;
    }

    LOG_DBG("Set strip_num_leds to %d, render_bit_depth to %d", strip_num_leds,
            render_bit_depth);
	
	led_strip_update(0, NULL);
}
