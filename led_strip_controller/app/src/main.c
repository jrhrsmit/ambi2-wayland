#include <zephyr/drivers/uart.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/toolchain.h>
#include <zephyr/usb/usb_device.h>

#include <zephyr/console/console.h>

#include "led.h"
#include "uart_console.h"
#include "usb_cdc_acm.h"

#include <zephyr/logging/log.h>
#include <zephyr/logging/log_ctrl.h>
LOG_MODULE_REGISTER(main, 3);

BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart),
             "Console device is not ACM CDC UART device");

static void print_help(int argc, char** argv);

void k_sys_fatal_error_handler(unsigned int reason, const z_arch_esf_t* esf) {
    LOG_ERR("Fatal system error (%d)", reason);
    LOG_INF("Rebooting in 1s...");
    LOG_PANIC();
    k_busy_wait(1000000);
    sys_reboot(SYS_REBOOT_COLD);
}

void reboot(int argc, char** argv) {
    LOG_INF("Rebooting in 1s...");
    LOG_PANIC();
    k_busy_wait(1000000);
    sys_reboot(SYS_REBOOT_COLD);
}

uart_console_cmd_t commands[] = {
    {.command = "a",
     .args = "<color0> <color1> ..",
     .description = "Set all LEDs in the LED strip and update it",
     .cmd_fn = led_set_all},
    {.command = "l",
     .args = "<num> <color>",
     .description = "Set LED <num> to color <color>",
     .cmd_fn = led_set},
    {.command = "u",
     .args = "-",
     .description = "Redraw LED colors on strip",
     .cmd_fn = led_strip_update},
    {.command = "led_set_brightness",
     .args = "<brightness>",
     .description = "Set global brightness level",
     .cmd_fn = led_set_brightness},
    {.command = "led_set_range",
     .args = "<start led> <num leds>",
     .description = "Set a range of LEDs. pass values separated with newlines",
     .cmd_fn = led_set_range},
    {.command = "led_set",
     .args = "<num> <color>",
     .description = "Set LED <num> to color <color>",
     .cmd_fn = led_set},
    {.command = "led_init",
     .args = "<strip size> <bit depth>",
     .description = "Initialize the LED strip with the size and bit depth",
     .cmd_fn = led_init},
    {.command = "led_strip_update",
     .args = "-",
     .description = "Redraw LED colors on strip",
     .cmd_fn = led_strip_update},
    {.command = "led_print_colors",
     .args = "-",
     .description = "Print current LED colors",
     .cmd_fn = led_print_colors},
    {.command = "help",
     .args = "-",
     .description = "Print this help message\0",
     .cmd_fn = print_help},
    {.command = "reboot\0",
     .args = "-\0",
     .description = "Reboot the system\0",
     .cmd_fn = reboot},
};

static void print_help(int argc, char** argv) {
    int num_commands = sizeof(commands) / sizeof(uart_console_cmd_t);
    printk("Available commands (%d):\n", num_commands);
    printk("\t%-*s%-*sDescirption\n", UART_CONSOLE_COMMAND_LEN, "Command",
           UART_CONSOLE_ARGS_LEN, "Arguments");
    for (int i = 0; i < num_commands; i++) {
        printk("\t%-*s%-*s%s\n", UART_CONSOLE_COMMAND_LEN, commands[i].command,
               UART_CONSOLE_ARGS_LEN, commands[i].args,
               commands[i].description);
    }
}

void main(void) {
    uart_console_init(commands, sizeof(commands) / sizeof(uart_console_cmd_t));
    usb_cdc_acm_init(uart_console_get());

    log_init();
    LOG_INF("R3.14 Ambi2-wayland led strip controller started. Using %d cores.",
            CONFIG_MP_NUM_CPUS);
}
