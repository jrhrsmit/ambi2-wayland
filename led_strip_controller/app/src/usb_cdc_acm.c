#include <stdio.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/logging/log.h>
#include <zephyr/logging/log_ctrl.h>
#include <zephyr/usb/usb_device.h>
LOG_MODULE_REGISTER(cdc_acm_echo, LOG_LEVEL_INF);

#include "uart_console.h"

static struct uart_console_t* uart_console;

static void interrupt_handler(const struct device* dev, void* user_data) {
    ARG_UNUSED(user_data);

    while (uart_irq_update(dev) && uart_irq_is_pending(dev)) {
        if (uart_irq_rx_ready(dev)) {
            int recv_len;
            static uint8_t buffer[UART_CONSOLE_LINE_LEN] = {0};
            static int buffer_pos = 0;
            size_t len = sizeof(buffer) - buffer_pos;

            recv_len = uart_fifo_read(dev, &buffer[buffer_pos], len);
            if (recv_len < 0) {
                LOG_ERR("Failed to read UART FIFO");
                recv_len = 0;
            };
            // buffer[buffer_pos + recv_len] = '\0';
            // printk("%s", &buffer[buffer_pos]);

            LOG_DBG("Available in read buf (%d new bytes): \"%s\"", recv_len,
                    buffer);

            //  if an entire line is entered, replace the newline character by a
            //  NULL character, and submit work
            char* cmd_start = buffer;
            int cmd_start_idx = 0;
            int cmd_len;
            for (int i = buffer_pos; i < buffer_pos + recv_len; i++) {
                if (buffer[i] == '\r') {
                    // mark it as the end of the string, and print a newline
                    // for echo
                    uart_console->cmd_fifo_queue_len++;
                    buffer[i] = '\0';

                    LOG_DBG("Queueing command %s", cmd_start);

                    cmd_len = i - cmd_start_idx;

                    // struct cmd_fifo_data* data =
                    //     k_calloc(sizeof(struct cmd_fifo_data), 1);
                    char* cmd = k_calloc(sizeof(char), cmd_len + 1);
                    if (cmd == NULL) {
                        LOG_ERR("Could not allocate %d bytes for command",
                                cmd_len + 1);
                    } else {
                        memcpy(cmd, cmd_start, cmd_len + 1);
                        if (k_fifo_alloc_put(&uart_console->cmd_fifo, cmd)) {
                            LOG_ERR(
                                "Could not queue command in FIFO (-ENOMEM)");
                        }
                    }
                    printk("%s.%d\n", strtok(cmd_start, " "),
                           uart_console->cmd_fifo_queue_len);

                    // setting start of next command
                    cmd_start_idx = i + 1;
                    cmd_start = &buffer[cmd_start_idx];
                }
            }
            // copy remaining incomplete command to start of buffer
            int i;
            for (i = 0; i < buffer_pos + recv_len - cmd_start_idx; i++) {
                buffer[i] = buffer[cmd_start_idx + i];
            }
            // terminate the string
            buffer[i] = '\0';
            // save the position
            buffer_pos = i;
            LOG_DBG("Remaining characters in buffer: \"%s\"", buffer);
            LOG_DBG("Buffer pos: %d", buffer_pos);
        }
    }
}

void usb_cdc_acm_init(struct uart_console_t* uart_console_dev) {
    const struct device* dev;
    uint32_t baudrate, dtr = 0U;
    int ret;
    uart_console = uart_console_dev;

    dev = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
    if (!device_is_ready(dev)) {
        LOG_ERR("CDC ACM device not ready");
        return;
    }

    ret = usb_enable(NULL);
    if (ret != 0) {
        LOG_ERR("Failed to enable USB");
        return;
    }

    LOG_INF("Wait for DTR");

    while (true) {
        uart_line_ctrl_get(dev, UART_LINE_CTRL_DTR, &dtr);
        if (dtr) {
            break;
        } else {
            /* Give CPU resources to low priority threads. */
            k_sleep(K_MSEC(100));
        }
    }

    LOG_INF("DTR set");

    /* They are optional, we use them to test the interrupt endpoint */
    ret = uart_line_ctrl_set(dev, UART_LINE_CTRL_DCD, 1);
    if (ret) {
        LOG_WRN("Failed to set DCD, ret code %d", ret);
    }

    ret = uart_line_ctrl_set(dev, UART_LINE_CTRL_DSR, 1);
    if (ret) {
        LOG_WRN("Failed to set DSR, ret code %d", ret);
    }

    /* Wait 1 sec for the host to do all settings */
    k_busy_wait(1000000);

    ret = uart_line_ctrl_get(dev, UART_LINE_CTRL_BAUD_RATE, &baudrate);
    if (ret) {
        LOG_WRN("Failed to get baudrate, ret code %d", ret);
    } else {
        LOG_INF("Baudrate detected: %d", baudrate);
    }

    uart_irq_callback_set(dev, interrupt_handler);

    /* Enable rx interrupts */
    uart_irq_rx_enable(dev);
}
