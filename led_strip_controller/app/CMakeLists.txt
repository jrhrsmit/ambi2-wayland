# SPDX-License-Identifier: Apache-2.0


cmake_minimum_required(VERSION 3.20.0)
set(exe_name "led_strip_controller")

list(APPEND BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR})
set(BOARD rpi_pico)

#find_package(Zephyr)
find_package(Zephyr REQUIRED HINTS ../zephyr)
project(${exe_name})

# header files
zephyr_include_directories(inc)

# source files
file(GLOB_RECURSE SRC "src/*.c")

target_sources(app PRIVATE ${SRC})
