# ambi2-wayland
Ambilight controller on Wayland.
Uses Raspberry Pi Pico and Zephyr for the serial port translation.
Largely copied from [grim](https://sr.ht/~emersion/grim/) and the Zephyr
samples.

# Tested solutions:
## Software scale
Scale down to the "resolution" defined by the LED strip.
```
buffer --> scale --> buffersink
```
800% cpu load. Too intense on CPU.

## Pixelize 
Keep the same resolution, but pixelize the entire screen, pixel size is the area defined by the LEDs `(params.width / params.strip_width) x (params.height / params.strip_height)`
```
buffer --> pixelize --> buffersink
```
250-300% cpu load. Doable, but actually too much.

## Pixelize borders
Only pixelize the edges:
```
buffer -- split ---------------------------> overlay_left --> overlay_top --> overlay_right --> overlay_bottom --> buffersink
             |----> crop_left ----> pixelize[0] ---|               |                |                  |
             |----> crop_top -----> pixelize[1] -------------------|                |                  |
             |----> crop_right ---> pixelize[2] ------------------------------------|                  |
             |----> crop_bottom --> pixelize[3] -------------------------------------------------------|
```
700% cpu load, heavier than just pixelize and about just as heavy as software scale.

# Useful commands for testing wf-recorder derived daemon
```
mpv /tmp/recording.avi --linear-upscaling --scale=nearest
```
```
./build/wf-recorder -x rgb0 -c rawvideo --file=/tmp/recording.avi
```
```
ffmpeg -hide_banner -v verbose -h filter=scale_vaapi
```
```
./build/wf-recorder -x rgb0 -c rawvideo --file=/tmp/recording.avi -P /dev/ttyACM0 -d /dev/dri/renderD128
```

# Test dmabuf-capture wlroots derived daemon
```
./build/dmabuf-capture -t vaapi -d /dev/dri/renderD128 -e libx264rgb -f bgr0 -r 12 /tmp/dmabuf_recording_01.mkv
```

## OpenCL
Deps:
```bash
pacman -S opencl-mesa clinfo
```
Run `clinfo` to check compatibility.  
Running with opencl:
``` 
ninja -C build && ./build/dmabuf-capture -t opencl -d 0 -e libx264rgb -f bgr0 -r 120 /tmp/dmabuf_test_opencl.mkv
```
