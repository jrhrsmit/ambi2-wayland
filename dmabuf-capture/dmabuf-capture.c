#define _POSIX_C_SOURCE 199309L
#include <drm_fourcc.h>
#include <libavcodec/avcodec.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavformat/avformat.h>
#include <libavutil/display.h>
#include <libavutil/hwcontext_drm.h>
#include <libavutil/opt.h>
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "wlr-export-dmabuf-unstable-v1-client-protocol.h"

// #include <CL/cl.h>

struct wayland_output {
    struct wl_list link;
    uint32_t id;
    struct wl_output* output;
    char* make;
    char* model;
    int width;
    int height;
    double framerate;
};

struct object {
    int fd;
    size_t size;
};

struct plane {
    uint32_t object_index;
    uint32_t offset;
    uint32_t stride;
};

struct capture_context {
    char* port;

    struct wl_display* display;
    struct wl_registry* registry;
    struct zwlr_export_dmabuf_manager_v1* export_manager;

    struct wl_list output_list;

    /* Target */
    struct wl_output* target_output;
    bool with_cursor;

    /* Main frame callback */
    struct zwlr_export_dmabuf_frame_v1* frame_callback;

    /* If something happens during capture */
    int err;
    bool quit;

    /* FFmpeg specific parts */
    pthread_t vid_thread;

    /* Config */
    char* out_filename;
    float out_bitrate;

    struct zwlr_export_dmabuf_frame_v1* frame;
    uint32_t format;
    uint32_t width;
    uint32_t height;
    uint32_t format_modifier;
    uint32_t num_objects;
    struct object objects[4];
    struct plane planes[1];
};

static void output_handle_geometry(void* data,
                                   struct wl_output* wl_output,
                                   int32_t x,
                                   int32_t y,
                                   int32_t phys_width,
                                   int32_t phys_height,
                                   int32_t subpixel,
                                   const char* make,
                                   const char* model,
                                   int32_t transform) {
    struct wayland_output* output = data;
    output->make = av_strdup(make);
    output->model = av_strdup(model);
}

static void output_handle_mode(void* data,
                               struct wl_output* wl_output,
                               uint32_t flags,
                               int32_t width,
                               int32_t height,
                               int32_t refresh) {
    if (flags & WL_OUTPUT_MODE_CURRENT) {
        struct wayland_output* output = data;
        output->width = width;
        output->height = height;
        output->framerate = (double)refresh / 1000.0;
    }
}

static void output_handle_done(void* data, struct wl_output* wl_output) {
    /* Nothing to do */
}

static void output_handle_scale(void* data,
                                struct wl_output* wl_output,
                                int32_t factor) {
    /* Nothing to do */
}

static const struct wl_output_listener output_listener = {
    .geometry = output_handle_geometry,
    .mode = output_handle_mode,
    .done = output_handle_done,
    .scale = output_handle_scale,
};

static void registry_handle_add(void* data,
                                struct wl_registry* reg,
                                uint32_t id,
                                const char* interface,
                                uint32_t ver) {
    struct capture_context* ctx = data;

    if (!strcmp(interface, wl_output_interface.name)) {
        struct wayland_output* output = malloc(sizeof(*output));

        output->id = id;
        output->output = wl_registry_bind(reg, id, &wl_output_interface, 1);

        wl_output_add_listener(output->output, &output_listener, output);
        wl_list_insert(&ctx->output_list, &output->link);
    }

    if (!strcmp(interface, zwlr_export_dmabuf_manager_v1_interface.name)) {
        ctx->export_manager = wl_registry_bind(
            reg, id, &zwlr_export_dmabuf_manager_v1_interface, 1);
    }
}

static void remove_output(struct wayland_output* out) {
    wl_list_remove(&out->link);
    free(out->make);
    free(out->model);
    free(out);
}

static struct wayland_output* find_output(struct capture_context* ctx,
                                          struct wl_output* out,
                                          int id) {
    struct wayland_output *output, *tmp;
    wl_list_for_each_safe(output, tmp, &ctx->output_list, link) {
        if (output->output == out || (id >= 0 && output->id == (uint32_t)id) ||
            id == -1) {
            return output;
        }
    }
    return NULL;
}

static void registry_handle_remove(void* data,
                                   struct wl_registry* reg,
                                   uint32_t id) {
    remove_output(find_output((struct capture_context*)data, NULL, id));
}

static const struct wl_registry_listener registry_listener = {
    .global = registry_handle_add,
    .global_remove = registry_handle_remove,
};

static void frame_free(void* opaque, struct capture_context* ctx) {
    for (size_t i = 0; i < ctx->num_objects; ++i) {
        close(ctx->objects[i].fd);
    }
    // zwlr_export_dmabuf_frame_v1_destroy(opaque);
}

static void frame_start(void* data,
                        struct zwlr_export_dmabuf_frame_v1* frame,
                        uint32_t width,
                        uint32_t height,
                        uint32_t offset_x,
                        uint32_t offset_y,
                        uint32_t buffer_flags,
                        uint32_t flags,
                        uint32_t format,
                        uint32_t mod_high,
                        uint32_t mod_low,
                        uint32_t num_objects) {
    struct capture_context* ctx = data;

    ctx->num_objects = num_objects;
    ctx->format = format;
    ctx->width = width;
    ctx->height = height;
    ctx->frame = frame;
    ctx->frame = frame;
    ctx->format_modifier = ((uint64_t)mod_high << 32) | mod_low;
}

static void frame_object(void* data,
                         struct zwlr_export_dmabuf_frame_v1* frame,
                         uint32_t index,
                         int32_t fd,
                         uint32_t size,
                         uint32_t offset,
                         uint32_t stride,
                         uint32_t plane_index) {
    struct capture_context* ctx = data;

    ctx->objects[index].fd = fd;
    ctx->objects[index].size = size;

    ctx->planes[plane_index].object_index = index;
    ctx->planes[plane_index].offset = offset;
    ctx->planes[plane_index].stride = stride;
}

static enum AVPixelFormat drm_fmt_to_pixfmt(uint32_t fmt) {
    switch (fmt) {
        case DRM_FORMAT_NV12:
            return AV_PIX_FMT_NV12;
        case DRM_FORMAT_ARGB8888:
            return AV_PIX_FMT_BGRA;
        case DRM_FORMAT_XRGB8888:
            return AV_PIX_FMT_BGR0;
        case DRM_FORMAT_ABGR8888:
            return AV_PIX_FMT_RGBA;
        case DRM_FORMAT_XBGR8888:
            return AV_PIX_FMT_RGB0;
        case DRM_FORMAT_RGBA8888:
            return AV_PIX_FMT_ABGR;
        case DRM_FORMAT_RGBX8888:
            return AV_PIX_FMT_0BGR;
        case DRM_FORMAT_BGRA8888:
            return AV_PIX_FMT_ARGB;
        case DRM_FORMAT_BGRX8888:
            return AV_PIX_FMT_0RGB;
        default:
            return AV_PIX_FMT_NONE;
    };
}

static void register_cb(struct capture_context* ctx);

static void frame_ready(void* data,
                        struct zwlr_export_dmabuf_frame_v1* frame,
                        uint32_t tv_sec_hi,
                        uint32_t tv_sec_lo,
                        uint32_t tv_nsec) {
    struct capture_context* ctx = data;
    enum AVPixelFormat pix_fmt = drm_fmt_to_pixfmt(ctx->format);
    int err = 0;

    /* TODO: support multiplane stuff */
    if (av_pix_fmt_count_planes(pix_fmt) != 1) {
        fprintf(stderr, "Cannot parse multiplane frame (%d planes)\n",
                av_pix_fmt_count_planes(pix_fmt));
        goto end;
    }
    printf("Frame ready :D\n");

    if (!ctx->quit && !ctx->err) {
        register_cb(ctx);
    }

end:
    ctx->err = err;
    frame_free(frame, ctx);
}

static void frame_cancel(void* data,
                         struct zwlr_export_dmabuf_frame_v1* frame,
                         uint32_t reason) {
    struct capture_context* ctx = data;
    fprintf(stderr, "Frame cancelled!\n");
    frame_free(frame, ctx);
    if (reason == ZWLR_EXPORT_DMABUF_FRAME_V1_CANCEL_REASON_PERMANENT) {
        fprintf(stderr, "Permanent failure, exiting\n");
        ctx->err = true;
    } else {
        register_cb(ctx);
    }
}

static const struct zwlr_export_dmabuf_frame_v1_listener frame_listener = {
    .frame = frame_start,
    .object = frame_object,
    .ready = frame_ready,
    .cancel = frame_cancel,
};

static void register_cb(struct capture_context* ctx) {
    ctx->frame_callback = zwlr_export_dmabuf_manager_v1_capture_output(
        ctx->export_manager, ctx->with_cursor, ctx->target_output);

    zwlr_export_dmabuf_frame_v1_add_listener(ctx->frame_callback,
                                             &frame_listener, ctx);
}

static void* vid_encode_thread(void* arg) {
    int err = 0;
    struct capture_context* ctx = arg;

    do {
        if (0)
            goto end;
    } while (!ctx->err);

end:
    if (!ctx->err) {
        ctx->err = err;
    }
    return NULL;
}

struct capture_context* q_ctx = NULL;

static void on_quit_signal(int signo) {
    q_ctx->quit = true;
}

static int main_loop(struct capture_context* ctx) {
    q_ctx = ctx;

    if (signal(SIGINT, on_quit_signal) == SIG_ERR) {
        fprintf(stderr, "Unable to install signal handler!\n");
        return AVERROR(EINVAL);
    }

    /* Start video encoding thread */
    pthread_create(&ctx->vid_thread, NULL, vid_encode_thread, ctx);

    /* Start the frame callback */
    register_cb(ctx);

    /* Run capture */
    while (wl_display_dispatch(ctx->display) != -1 && !ctx->err && !ctx->quit)
        ;

    /* Join with encoder thread */
    pthread_join(ctx->vid_thread, NULL);

    // TODO: close port, exit gracefully

    return ctx->err;
}

static int init(struct capture_context* ctx) {
    ctx->display = wl_display_connect(NULL);
    if (!ctx->display) {
        fprintf(stderr, "Failed to connect to display!\n");
        return -EINVAL;
    }

    wl_list_init(&ctx->output_list);

    ctx->registry = wl_display_get_registry(ctx->display);
    wl_registry_add_listener(ctx->registry, &registry_listener, ctx);

    // First roundtrip to fetch globals
    wl_display_roundtrip(ctx->display);

    // Second roundtrip to fetch wl_output information
    wl_display_roundtrip(ctx->display);

    if (!ctx->export_manager) {
        fprintf(stderr, "Compositor doesn't support %s!\n",
                zwlr_export_dmabuf_manager_v1_interface.name);
        return -ENOTSUP;
    }

    return 0;
}

static void uninit(struct capture_context* ctx);

static const char usage[] =
    "usage: dmabuf-capture [options...] <destination port>\n"
    "  -o <output ID>\n"
    "  -r <bitrate in Mbps>\n"
    "\n"
    "Example:\n"
    "  dmabuf-capture -o 41 -r 115200 /dev/ttyUSB0\n";

int main(int argc, char* argv[]) {
    struct capture_context ctx = {
        .out_bitrate = 115200,
    };
    int output_id = -1;
    int opt;
    while ((opt = getopt(argc, argv, "ho:r:")) != -1) {
        char* end;
        switch (opt) {
            case 'o':
                output_id = strtol(optarg, &end, 10);
                if (optarg[0] == '\0' || end[0] != '\0') {
                    fprintf(stderr, "Output ID is not an integer\n");
                    return 1;
                }
                break;
            case 'r':
                ctx.out_bitrate = strtol(optarg, &end, 10);
                if (optarg[0] == '\0' || end[0] != '\0') {
                    fprintf(stderr, "Bitrate is not an integer\n");
                    return 1;
                }
            default:
                fprintf(stderr, "%s", usage);
                return 1;
        }
    }

    if (optind >= argc) {
        fprintf(stderr, "Missing port argument\n");
        fprintf(stderr, "%s", usage);
        return 1;
    }
    ctx.port = argv[optind];

    int err = init(&ctx);
    if (err) {
        goto end;
    }

    struct wayland_output *o, *tmp_o;
    wl_list_for_each_reverse_safe(o, tmp_o, &ctx.output_list, link) {
        printf("Capturable output: %s Model: %s: ID: %i\n", o->make, o->model,
               o->id);
    }

    o = find_output(&ctx, NULL, output_id);
    if (!o) {
        printf("Unable to find output with ID %d\n", output_id);
        return 1;
    }

    ctx.target_output = o->output;
    ctx.with_cursor = true;

    err = main_loop(&ctx);
    if (err) {
        goto end;
    }

end:
    uninit(&ctx);
    return err;
}

static void uninit(struct capture_context* ctx) {
    struct wayland_output *output, *tmp_o;
    wl_list_for_each_safe(output, tmp_o, &ctx->output_list, link) {
        remove_output(output);
    }

    if (ctx->export_manager) {
        zwlr_export_dmabuf_manager_v1_destroy(ctx->export_manager);
    }
}
